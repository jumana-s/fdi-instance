gateway_name = "minio-gateway"
resource_group_name = "fdidatalakeblob"
storage_account_name = "fditerraformtest"

argocd_namespace = "argocd"
kubernetes_namespace = "minio"

# The actual data
datasets = {
  cropimaging = {

    kubeflow_readers = ["blair", "jim"]
    kubeflow_writers = ["blair"]

    metadata = {
      division = "DScD"
      use_case = "crop imaging"
      contact_email = "blair.drummond@canada.ca"
      cct_score = 0
    }
  }
  frontiercounts = {

    kubeflow_readers = ["jim"]
    kubeflow_writers = []

    metadata = {
      division = "CCTTS"
      use_case = "fc"
      contact_email = "blair.drummond@canada.ca"
      cct_score = 5
    }
  }
}
