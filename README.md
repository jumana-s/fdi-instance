# fdi-instance

![screenshot](docs/screenshot.png)

Create:

- A Resource group (if desired)
- A storage account
- A MinIO-Gateway via ArgoCD (deploying a helm chart)

And, given a list of "datasets"

- A set of Azure blob containers / MinIO buckets
- MinIO IAM policies for the users who are allowed to use those buckets.

## Known issues:

Approving a new ingress is (probably?) a manual process, so you cannot create a service and push iam policies to its URL in one step. As a result, the easiest way to run this module is

- Apply this module with no datasets (so that no IAM policies get applied)
- Wait for everything to come up in ArgoCD
- Port-forward the MinIO gateway to localhost:9000
- Add your datasets and run terraform apply

**In Gitlab CI: This might require either waiting for the ingress to become available or doing port-forwards within the runner**
