module "containers" {

  for_each = var.datasets

  source = "git::https://github.com/blairdrummond/terraform-test-azurerm-container?ref=15fd013f110961d35e412a0af72aeb81e6f9437a"

  container_name = each.key
  metadata = each.value.metadata

  storage_account_name = module.storage_account.name

}


module "minio_iam" {

  source = "git::https://gitlab.com/happylittlecloud/terraform/modules/fdi-datasets?ref=9c49fc9ada771aa877fd88e2c09ceb054affffe7"
  minio_server = var.minio_server_url
  minio_access_key = module.minio_gateway.access_key
  minio_secret_key = module.minio_gateway.secret_key

  # Subset down to just the iam stuff
  datasets = {
    for k,v in var.datasets: k => {
      kubeflow_readers = v.kubeflow_readers
      kubeflow_writers = v.kubeflow_writers
    }
  }
}
