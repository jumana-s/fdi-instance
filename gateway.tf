resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = "canadacentral"
}


module "storage_account" {
  source = "git::https://gitlab.com/happylittlecloud/terraform/modules/storage-account-module?ref=a41426248a836fec7a57460308c3e47a96eb55b6"

  storage_account_name = var.storage_account_name
  resource_group_name = azurerm_resource_group.rg.name
}


module "minio_gateway" {

  source = "git::https://gitlab.com/happylittlecloud/terraform/modules/minio-gateway-module?ref=7af0f525bce9810988bbc9303c95fe7c5a48211a"

  azure_storage_account_name = module.storage_account.name
  azure_storage_account_key = module.storage_account.access_key

  argocd_namespace = var.argocd_namespace
  name = var.gateway_name
  namespace = var.kubernetes_namespace

  helm_chart_repo = "https://blairdrummond.github.io/charts"
  helm_chart_name = "minio-gateway"
  helm_chart_version = "0.0.1"
  minio_replicas = 3
  etcd_replicas = 3
  # minio_image_repo = "docker.io"
  # minio_image_name = "bitnami/minio"
  # minio_image_tag = "latest"

}
